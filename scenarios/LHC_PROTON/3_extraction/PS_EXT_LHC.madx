/**********************************************************************************
*
* MAD-X input script for the flat top optics of the LHC cycle.
* 22/07/2019 - Alexander Huschauer
************************************************************************************/
 
/******************************************************************
 * Energy and particle type definition
 ******************************************************************/

BEAM, PARTICLE=PROTON, PC = 26.40;
BRHO      := BEAM->PC * 3.3356;

/******************************************************************
 * Call lattice files
 ******************************************************************/

call, file="../../../PS_MU.seq";
call, file="../../../PS_SS.seq";
call, file="../../../PS.str";
call, file="./PS_EXT_LHC.str";

/******************************************************************
 * PTC Twiss
 ******************************************************************/

use, sequence=PS;
select, flag=ptc_twiss, column=name,keyword,s,x,px,y,py,t,pt,beta11,alfa11,beta22,alfa22,disp1,disp,disp3,disp4,gamma11,gamma22,mu1,mu2,energy,l,angle,K0L,K0SL,K1L,K1SL,K2L,K2SL,K3L,K3SL,K4L,K4SL,K5L,K5SL,VKICK,HKICK,SLOT_ID;
ptc_create_universe;
ptc_create_layout, model=2, method=6, nst=5, exact=true,;
ptc_twiss,closed_orbit,icase=56,no=4, file = './PS_EXT_LHC.tfs';
ptc_end;

!stop;

! obtain smooth optics functions by slicing the elements
use, sequence=PS;
select, flag=ptc_twiss, column=name,keyword,s,x,px,y,py,t,pt,beta11,alfa11,beta22,alfa22,disp1,disp,disp3,disp4,gamma11,gamma22,mu1,mu2,energy,l,angle,K0L,K0SL,K1L,K1SL,K2L,K2SL,K3L,K3SL,K4L,K4SL,K5L,K5SL,VKICK,HKICK,SLOT_ID;
ptc_create_universe;
ptc_create_layout, model=2, method=6, nst=5, exact=true,;
ptc_twiss,closed_orbit,icase=56,no=4,slice_magnets=true, file = './PS_EXT_LHC_interpolated.tfs';
ptc_end;