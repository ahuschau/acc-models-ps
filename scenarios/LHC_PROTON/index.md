<h1> LHC PROTON optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the LHC physics programme. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the LHC PROTON cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.21</td>
    <td align="center">6.248</td>
    <td align="center">0.72</td>
    <td align="center">-2.85</td>
    
    <td align="center">334.372</td>
    <td align="center">12.921</td>
    <td align="center">21.37</td>
    <td align="center">2.3</td>
    
    <td align="center">397.204</td>
    <td align="center">12.498</td>
    <td align="center">22.283</td>
    <td align="center">2.367</td>
    
    <td align="center">403.207</td>
    <td align="center">22.126</td>
    <td align="center">11.914</td>
    <td align="center">3.131</td>
    
    <td align="center">422.616</td>
    <td align="center">13.183</td>
    <td align="center">21.121</td>
    <td align="center">2.379</td>
    
    <td align="center">528.871</td>
    <td align="center">23.198</td>
    <td align="center">11.331</td>
    <td align="center">3.121</td>
    
    <td align="center">510.833</td>
    <td align="center">11.503</td>
    <td align="center">22.672</td>
    <td align="center">2.399</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.21</td>
    <td align="center">6.245</td>
    <td align="center">0.73</td>
    <td align="center">-2.87</td>
    
    <td align="center">334.372</td>
    <td align="center">12.72</td>
    <td align="center">21.536</td>
    <td align="center">2.287</td>
    
    <td align="center">397.204</td>
    <td align="center">12.568</td>
    <td align="center">21.92</td>
    <td align="center">2.367</td>
    
    <td align="center">403.207</td>
    <td align="center">22.451</td>
    <td align="center">11.706</td>
    <td align="center">3.122</td>
    
    <td align="center">422.616</td>
    <td align="center">13.095</td>
    <td align="center">21.485</td>
    <td align="center">2.359</td>
    
    <td align="center">528.871</td>
    <td align="center">22.864</td>
    <td align="center">11.529</td>
    <td align="center">3.095</td>
    
    <td align="center">510.833</td>
    <td align="center">11.679</td>
    <td align="center">22.512</td>
    <td align="center">2.386</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">25.48</td>
    <td align="center">26.42</td>
    <td align="center">28.15</td>
    <td align="center">1.0</td>
    <td align="center">26.4</td>
    <td align="center">6.217</td>
    <td align="center">6.28</td>
    <td align="center">1.67</td>
    <td align="center">0.44</td>
    
    <td align="center">334.372</td>
    <td align="center">12.655</td>
    <td align="center">21.972</td>
    <td align="center">2.329</td>
    
    <td align="center">397.204</td>
    <td align="center">12.655</td>
    <td align="center">21.972</td>
    <td align="center">2.329</td>
    
    <td align="center">403.207</td>
    <td align="center">22.607</td>
    <td align="center">11.697</td>
    <td align="center">3.074</td>
    
    <td align="center">422.616</td>
    <td align="center">12.655</td>
    <td align="center">21.972</td>
    <td align="center">2.329</td>
    
    <td align="center">528.871</td>
    <td align="center">22.607</td>
    <td align="center">11.697</td>
    <td align="center">3.074</td>
    
    <td align="center">510.833</td>
    <td align="center">11.81</td>
    <td align="center">22.356</td>
    <td align="center">2.329</td>
    <tr>
    <td> <a href="3_extraction/index.html">extraction</a></td>
    <td align="center">25.48</td>
    <td align="center">26.42</td>
    <td align="center">28.15</td>
    <td align="center">1.0</td>
    <td align="center">26.4</td>
    <td align="center">6.218</td>
    <td align="center">6.285</td>
    <td align="center">1.93</td>
    <td align="center">0.03</td>
    
    <td align="center">334.372</td>
    <td align="center">26.123</td>
    <td align="center">18.368</td>
    <td align="center">4.13</td>
    
    <td align="center">397.204</td>
    <td align="center">19.798</td>
    <td align="center">16.905</td>
    <td align="center">1.54</td>
    
    <td align="center">403.207</td>
    <td align="center">22.406</td>
    <td align="center">12.007</td>
    <td align="center">2.926</td>
    
    <td align="center">422.616</td>
    <td align="center">12.814</td>
    <td align="center">29.717</td>
    <td align="center">4.11</td>
    
    <td align="center">528.871</td>
    <td align="center">34.434</td>
    <td align="center">12.787</td>
    <td align="center">5.666</td>
    
    <td align="center">510.833</td>
    <td align="center">6.173</td>
    <td align="center">30.799</td>
    <td align="center">3.012</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](BCMS_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>