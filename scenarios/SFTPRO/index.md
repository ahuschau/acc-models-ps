<h1> SFTPRO optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the SPS fixed target physics programme. At flat top, the Multi-Turn Extraction scheme is used to split the beam in the horizontal phase space into the core and four islands. Subsequently, the beam is extracted over five turns. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the SFTPRO cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.153</td>
    <td align="center">6.263</td>
    <td align="center">-5.24</td>
    <td align="center">-7.1</td>
    
    <td align="center">334.372</td>
    <td align="center">12.744</td>
    <td align="center">21.746</td>
    <td align="center">2.589</td>
    
    <td align="center">397.204</td>
    <td align="center">12.356</td>
    <td align="center">22.067</td>
    <td align="center">2.313</td>
    
    <td align="center">403.207</td>
    <td align="center">22.037</td>
    <td align="center">11.784</td>
    <td align="center">3.141</td>
    
    <td align="center">422.616</td>
    <td align="center">13.635</td>
    <td align="center">21.185</td>
    <td align="center">2.566</td>
    
    <td align="center">528.871</td>
    <td align="center">23.028</td>
    <td align="center">11.59</td>
    <td align="center">3.458</td>
    
    <td align="center">510.833</td>
    <td align="center">11.422</td>
    <td align="center">22.584</td>
    <td align="center">2.448</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.153</td>
    <td align="center">6.263</td>
    <td align="center">-5.23</td>
    <td align="center">-7.13</td>
    
    <td align="center">334.372</td>
    <td align="center">12.731</td>
    <td align="center">21.803</td>
    <td align="center">2.511</td>
    
    <td align="center">397.204</td>
    <td align="center">12.349</td>
    <td align="center">22.015</td>
    <td align="center">2.395</td>
    
    <td align="center">403.207</td>
    <td align="center">22.04</td>
    <td align="center">11.744</td>
    <td align="center">3.235</td>
    
    <td align="center">422.616</td>
    <td align="center">13.638</td>
    <td align="center">21.247</td>
    <td align="center">2.551</td>
    
    <td align="center">528.871</td>
    <td align="center">23.022</td>
    <td align="center">11.633</td>
    <td align="center">3.422</td>
    
    <td align="center">510.833</td>
    <td align="center">11.431</td>
    <td align="center">22.538</td>
    <td align="center">2.494</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">13.09</td>
    <td align="center">14.03</td>
    <td align="center">14.95</td>
    <td align="center">1.0</td>
    <td align="center">14.0</td>
    <td align="center">6.247</td>
    <td align="center">6.298</td>
    <td align="center">3.93</td>
    <td align="center">1.91</td>
    
    <td align="center">334.372</td>
    <td align="center">12.583</td>
    <td align="center">21.944</td>
    <td align="center">2.306</td>
    
    <td align="center">397.204</td>
    <td align="center">12.583</td>
    <td align="center">21.944</td>
    <td align="center">2.306</td>
    
    <td align="center">403.207</td>
    <td align="center">22.521</td>
    <td align="center">11.652</td>
    <td align="center">3.047</td>
    
    <td align="center">422.616</td>
    <td align="center">12.583</td>
    <td align="center">21.945</td>
    <td align="center">2.306</td>
    
    <td align="center">528.871</td>
    <td align="center">22.521</td>
    <td align="center">11.652</td>
    <td align="center">3.047</td>
    
    <td align="center">510.833</td>
    <td align="center">11.743</td>
    <td align="center">22.318</td>
    <td align="center">2.306</td>
    <tr>
    <td> <a href="3_resonance_crossing/index.html">resonance crossing </a></td>
    <td align="center">13.09</td>
    <td align="center">14.03</td>
    <td align="center">14.95</td>
    <td align="center">1.0</td>
    <td align="center">14.0</td>
    <td align="center">6.25</td>
    <td align="center">6.298</td>
    <td align="center">1.03</td>
    <td align="center">3.48</td>
    
    <td align="center">334.372</td>
    <td align="center">12.583</td>
    <td align="center">21.946</td>
    <td align="center">2.301</td>
    
    <td align="center">397.204</td>
    <td align="center">12.586</td>
    <td align="center">21.944</td>
    <td align="center">2.303</td>
    
    <td align="center">403.207</td>
    <td align="center">22.525</td>
    <td align="center">11.651</td>
    <td align="center">3.043</td>
    
    <td align="center">422.616</td>
    <td align="center">12.561</td>
    <td align="center">21.962</td>
    <td align="center">2.302</td>
    
    <td align="center">528.871</td>
    <td align="center">22.514</td>
    <td align="center">11.653</td>
    <td align="center">3.04</td>
    
    <td align="center">510.833</td>
    <td align="center">11.747</td>
    <td align="center">22.316</td>
    <td align="center">2.302</td>
    <tr>
    <td> <a href="4_horizontal_splitting/index.html">horizontal splitting </a></td>
    <td align="center">13.09</td>
    <td align="center">14.03</td>
    <td align="center">14.95</td>
    <td align="center">1.0</td>
    <td align="center">14.0</td>
    <td align="center">6.255</td>
    <td align="center">6.298</td>
    <td align="center">1.01</td>
    <td align="center">3.46</td>
    
    <td align="center">334.372</td>
    <td align="center">12.584</td>
    <td align="center">21.95</td>
    <td align="center">2.293</td>
    
    <td align="center">397.204</td>
    <td align="center">12.592</td>
    <td align="center">21.943</td>
    <td align="center">2.298</td>
    
    <td align="center">403.207</td>
    <td align="center">22.533</td>
    <td align="center">11.65</td>
    <td align="center">3.035</td>
    
    <td align="center">422.616</td>
    <td align="center">12.517</td>
    <td align="center">21.997</td>
    <td align="center">2.294</td>
    
    <td align="center">528.871</td>
    <td align="center">22.5</td>
    <td align="center">11.654</td>
    <td align="center">3.026</td>
    
    <td align="center">510.833</td>
    <td align="center">11.753</td>
    <td align="center">22.313</td>
    <td align="center">2.295</td>
    <tr>
    <td> <a href="5_phase_rotation/index.html">phase rotation </a></td>
    <td align="center">13.09</td>
    <td align="center">14.03</td>
    <td align="center">14.95</td>
    <td align="center">1.0</td>
    <td align="center">14.0</td>
    <td align="center">6.266</td>
    <td align="center">6.298</td>
    <td align="center">-0.49</td>
    <td align="center">4.17</td>
    
    <td align="center">334.372</td>
    <td align="center">12.588</td>
    <td align="center">21.957</td>
    <td align="center">2.275</td>
    
    <td align="center">397.204</td>
    <td align="center">12.603</td>
    <td align="center">21.942</td>
    <td align="center">2.289</td>
    
    <td align="center">403.207</td>
    <td align="center">22.545</td>
    <td align="center">11.647</td>
    <td align="center">3.019</td>
    
    <td align="center">422.616</td>
    <td align="center">12.43</td>
    <td align="center">22.068</td>
    <td align="center">2.28</td>
    
    <td align="center">528.871</td>
    <td align="center">22.474</td>
    <td align="center">11.656</td>
    <td align="center">3.0</td>
    
    <td align="center">510.833</td>
    <td align="center">11.765</td>
    <td align="center">22.305</td>
    <td align="center">2.281</td>
    <tr>
    <td> <a href="6_extraction/index.html">extraction</a></td>
    <td align="center">13.09</td>
    <td align="center">14.03</td>
    <td align="center">14.95</td>
    <td align="center">1.0</td>
    <td align="center">14.0</td>
    <td align="center">6.266</td>
    <td align="center">6.299</td>
    <td align="center">-0.55</td>
    <td align="center">4.11</td>
    
    <td align="center">334.372</td>
    <td align="center">12.767</td>
    <td align="center">22.388</td>
    <td align="center">2.315</td>
    
    <td align="center">397.204</td>
    <td align="center">12.452</td>
    <td align="center">21.44</td>
    <td align="center">2.233</td>
    
    <td align="center">403.207</td>
    <td align="center">22.193</td>
    <td align="center">11.275</td>
    <td align="center">2.947</td>
    
    <td align="center">422.616</td>
    <td align="center">12.54</td>
    <td align="center">22.648</td>
    <td align="center">2.271</td>
    
    <td align="center">528.871</td>
    <td align="center">22.781</td>
    <td align="center">12.006</td>
    <td align="center">3.009</td>
    
    <td align="center">510.833</td>
    <td align="center">11.662</td>
    <td align="center">21.966</td>
    <td align="center">2.244</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](MTE_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>