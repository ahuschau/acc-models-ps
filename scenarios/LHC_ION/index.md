<h1> LHC ION optics</h1>

<h2> Description </h2>

<p> Operational scenario for the lead ion beams produced for the LHC physics programme. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the LHC ION cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">0.07</td>
    <td align="center">1.01</td>
    <td align="center">1.08</td>
    <td align="center">0.38</td>
    <td align="center">0.37</td>
    <td align="center">6.21</td>
    <td align="center">6.245</td>
    <td align="center">-5.33</td>
    <td align="center">-7.11</td>
    
    <td align="center">334.372</td>
    <td align="center">12.627</td>
    <td align="center">21.805</td>
    <td align="center">2.317</td>
    
    <td align="center">397.204</td>
    <td align="center">12.496</td>
    <td align="center">22.041</td>
    <td align="center">2.351</td>
    
    <td align="center">403.207</td>
    <td align="center">22.34</td>
    <td align="center">11.756</td>
    <td align="center">3.091</td>
    
    <td align="center">422.616</td>
    <td align="center">13.102</td>
    <td align="center">21.562</td>
    <td align="center">2.321</td>
    
    <td align="center">528.871</td>
    <td align="center">22.72</td>
    <td align="center">11.636</td>
    <td align="center">3.085</td>
    
    <td align="center">510.833</td>
    <td align="center">11.627</td>
    <td align="center">22.551</td>
    <td align="center">2.346</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">0.07</td>
    <td align="center">1.01</td>
    <td align="center">1.08</td>
    <td align="center">0.38</td>
    <td align="center">0.37</td>
    <td align="center">6.21</td>
    <td align="center">6.245</td>
    <td align="center">-5.27</td>
    <td align="center">-7.2</td>
    
    <td align="center">334.372</td>
    <td align="center">12.627</td>
    <td align="center">21.807</td>
    <td align="center">2.355</td>
    
    <td align="center">397.204</td>
    <td align="center">12.494</td>
    <td align="center">22.028</td>
    <td align="center">2.353</td>
    
    <td align="center">403.207</td>
    <td align="center">22.337</td>
    <td align="center">11.751</td>
    <td align="center">3.122</td>
    
    <td align="center">422.616</td>
    <td align="center">13.102</td>
    <td align="center">21.577</td>
    <td align="center">2.383</td>
    
    <td align="center">528.871</td>
    <td align="center">22.719</td>
    <td align="center">11.643</td>
    <td align="center">3.162</td>
    
    <td align="center">510.833</td>
    <td align="center">11.626</td>
    <td align="center">22.552</td>
    <td align="center">2.385</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">6.02</td>
    <td align="center">6.96</td>
    <td align="center">7.42</td>
    <td align="center">0.99</td>
    <td align="center">6.85</td>
    <td align="center">6.2379999999999995</td>
    <td align="center">6.274</td>
    <td align="center">0.78</td>
    <td align="center">-0.07</td>
    
    <td align="center">334.372</td>
    <td align="center">12.61</td>
    <td align="center">21.998</td>
    <td align="center">2.313</td>
    
    <td align="center">397.204</td>
    <td align="center">12.61</td>
    <td align="center">21.998</td>
    <td align="center">2.313</td>
    
    <td align="center">403.207</td>
    <td align="center">22.536</td>
    <td align="center">11.704</td>
    <td align="center">3.054</td>
    
    <td align="center">422.616</td>
    <td align="center">12.609</td>
    <td align="center">21.998</td>
    <td align="center">2.313</td>
    
    <td align="center">528.871</td>
    <td align="center">22.536</td>
    <td align="center">11.704</td>
    <td align="center">3.054</td>
    
    <td align="center">510.833</td>
    <td align="center">11.769</td>
    <td align="center">22.388</td>
    <td align="center">2.313</td>
    <tr>
    <td> <a href="3_extraction/index.html">extraction</a></td>
    <td align="center">6.02</td>
    <td align="center">6.96</td>
    <td align="center">7.42</td>
    <td align="center">0.99</td>
    <td align="center">6.85</td>
    <td align="center">6.2379999999999995</td>
    <td align="center">6.282</td>
    <td align="center">-0.03</td>
    <td align="center">-0.18</td>
    
    <td align="center">334.372</td>
    <td align="center">24.742</td>
    <td align="center">18.299</td>
    <td align="center">3.907</td>
    
    <td align="center">397.204</td>
    <td align="center">19.051</td>
    <td align="center">17.171</td>
    <td align="center">1.715</td>
    
    <td align="center">403.207</td>
    <td align="center">22.046</td>
    <td align="center">12.049</td>
    <td align="center">3.093</td>
    
    <td align="center">422.616</td>
    <td align="center">12.612</td>
    <td align="center">29.277</td>
    <td align="center">3.982</td>
    
    <td align="center">528.871</td>
    <td align="center">33.886</td>
    <td align="center">12.7</td>
    <td align="center">5.42</td>
    
    <td align="center">510.833</td>
    <td align="center">6.264</td>
    <td align="center">30.467</td>
    <td align="center">3.082</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](ION_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>