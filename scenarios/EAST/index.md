<h1> EAST optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the EAST area. At flat top, the beam is debunched and extracted using a resonant slow extraction. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the EAST cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.192</td>
    <td align="center">6.295</td>
    <td align="center">-5.3</td>
    <td align="center">-7.13</td>
    
    <td align="center">334.372</td>
    <td align="center">12.62</td>
    <td align="center">21.911</td>
    <td align="center">2.473</td>
    
    <td align="center">397.204</td>
    <td align="center">12.522</td>
    <td align="center">22.028</td>
    <td align="center">2.297</td>
    
    <td align="center">403.207</td>
    <td align="center">22.4</td>
    <td align="center">11.73</td>
    <td align="center">3.074</td>
    
    <td align="center">422.616</td>
    <td align="center">13.014</td>
    <td align="center">21.61</td>
    <td align="center">2.423</td>
    
    <td align="center">528.871</td>
    <td align="center">22.677</td>
    <td align="center">11.63</td>
    <td align="center">3.254</td>
    
    <td align="center">510.833</td>
    <td align="center">11.654</td>
    <td align="center">22.43</td>
    <td align="center">2.364</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.192</td>
    <td align="center">6.294</td>
    <td align="center">-5.29</td>
    <td align="center">-7.15</td>
    
    <td align="center">334.372</td>
    <td align="center">12.61</td>
    <td align="center">21.98</td>
    <td align="center">2.421</td>
    
    <td align="center">397.204</td>
    <td align="center">12.515</td>
    <td align="center">21.984</td>
    <td align="center">2.348</td>
    
    <td align="center">403.207</td>
    <td align="center">22.4</td>
    <td align="center">11.69</td>
    <td align="center">3.131</td>
    
    <td align="center">422.616</td>
    <td align="center">13.018</td>
    <td align="center">21.665</td>
    <td align="center">2.408</td>
    
    <td align="center">528.871</td>
    <td align="center">22.672</td>
    <td align="center">11.674</td>
    <td align="center">3.221</td>
    
    <td align="center">510.833</td>
    <td align="center">11.661</td>
    <td align="center">22.377</td>
    <td align="center">2.385</td>
    <tr>
    <td> <a href="2_start_flat_top/index.html">start flat top </a></td>
    <td align="center">23.08</td>
    <td align="center">24.02</td>
    <td align="center">25.6</td>
    <td align="center">1.0</td>
    <td align="center">24.0</td>
    <td align="center">6.228</td>
    <td align="center">6.32</td>
    <td align="center">0.19</td>
    <td align="center">1.05</td>
    
    <td align="center">334.372</td>
    <td align="center">12.622</td>
    <td align="center">21.881</td>
    <td align="center">2.32</td>
    
    <td align="center">397.204</td>
    <td align="center">12.622</td>
    <td align="center">21.881</td>
    <td align="center">2.32</td>
    
    <td align="center">403.207</td>
    <td align="center">22.598</td>
    <td align="center">11.613</td>
    <td align="center">3.066</td>
    
    <td align="center">422.616</td>
    <td align="center">12.622</td>
    <td align="center">21.882</td>
    <td align="center">2.32</td>
    
    <td align="center">528.871</td>
    <td align="center">22.598</td>
    <td align="center">11.613</td>
    <td align="center">3.066</td>
    
    <td align="center">510.833</td>
    <td align="center">11.778</td>
    <td align="center">22.239</td>
    <td align="center">2.32</td>
    <tr>
    <td> <a href="3_flat_top_without_QSE_and_XSE/index.html">flat top without QSE and XSE </a></td>
    <td align="center">23.08</td>
    <td align="center">24.02</td>
    <td align="center">25.6</td>
    <td align="center">1.0</td>
    <td align="center">24.0</td>
    <td align="center">6.224</td>
    <td align="center">6.287</td>
    <td align="center">-10.02</td>
    <td align="center">-2.19</td>
    
    <td align="center">334.372</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">397.204</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">403.207</td>
    <td align="center">22.59</td>
    <td align="center">11.681</td>
    <td align="center">3.068</td>
    
    <td align="center">422.616</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">528.871</td>
    <td align="center">22.59</td>
    <td align="center">11.681</td>
    <td align="center">3.068</td>
    
    <td align="center">510.833</td>
    <td align="center">11.794</td>
    <td align="center">22.339</td>
    <td align="center">2.323</td>
    <tr>
    <td> <a href="4_slow_extraction/index.html">slow extraction </a></td>
    <td align="center">23.08</td>
    <td align="center">24.02</td>
    <td align="center">25.6</td>
    <td align="center">1.0</td>
    <td align="center">24.0</td>
    <td align="center">6.224</td>
    <td align="center">6.287</td>
    <td align="center">-2.3</td>
    <td align="center">-6.41</td>
    
    <td align="center">334.372</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">397.204</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">403.207</td>
    <td align="center">22.59</td>
    <td align="center">11.681</td>
    <td align="center">3.068</td>
    
    <td align="center">422.616</td>
    <td align="center">12.638</td>
    <td align="center">21.959</td>
    <td align="center">2.323</td>
    
    <td align="center">528.871</td>
    <td align="center">22.59</td>
    <td align="center">11.681</td>
    <td align="center">3.068</td>
    
    <td align="center">510.833</td>
    <td align="center">11.794</td>
    <td align="center">22.339</td>
    <td align="center">2.323</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](EAST2_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>