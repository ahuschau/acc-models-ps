<h1> Bare machine optics</h1>

<h2> Description </h2>

<p> The optics are determined by the combined function magnets only, i.e. LEQ or PFW are not in use.  </p>


<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_proton_injection_energy/index.html">proton injection energy </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.245</td>
    <td align="center">6.284</td>
    <td align="center">-5.35</td>
    <td align="center">-7.16</td>
    
    <td align="center">334.372</td>
    <td align="center">12.591</td>
    <td align="center">21.979</td>
    <td align="center">2.307</td>
    
    <td align="center">397.204</td>
    <td align="center">12.591</td>
    <td align="center">21.979</td>
    <td align="center">2.307</td>
    
    <td align="center">403.207</td>
    <td align="center">22.518</td>
    <td align="center">11.683</td>
    <td align="center">3.048</td>
    
    <td align="center">422.616</td>
    <td align="center">12.591</td>
    <td align="center">21.979</td>
    <td align="center">2.307</td>
    
    <td align="center">528.871</td>
    <td align="center">22.518</td>
    <td align="center">11.683</td>
    <td align="center">3.048</td>
    
    <td align="center">510.833</td>
    <td align="center">11.751</td>
    <td align="center">22.363</td>
    <td align="center">2.307</td>
    <tr>
    <td> <a href="1_ion_injection_energy/index.html">ion injection energy </a></td>
    <td align="center">0.07</td>
    <td align="center">1.01</td>
    <td align="center">1.08</td>
    <td align="center">0.38</td>
    <td align="center">77.65</td>
    <td align="center">6.247</td>
    <td align="center">6.279</td>
    <td align="center">-5.33</td>
    <td align="center">-7.22</td>
    
    <td align="center">334.372</td>
    <td align="center">12.589</td>
    <td align="center">21.991</td>
    <td align="center">2.306</td>
    
    <td align="center">397.204</td>
    <td align="center">12.589</td>
    <td align="center">21.991</td>
    <td align="center">2.306</td>
    
    <td align="center">403.207</td>
    <td align="center">22.511</td>
    <td align="center">11.692</td>
    <td align="center">3.046</td>
    
    <td align="center">422.616</td>
    <td align="center">12.589</td>
    <td align="center">21.991</td>
    <td align="center">2.306</td>
    
    <td align="center">528.871</td>
    <td align="center">22.511</td>
    <td align="center">11.692</td>
    <td align="center">3.046</td>
    
    <td align="center">510.833</td>
    <td align="center">11.749</td>
    <td align="center">22.378</td>
    <td align="center">2.306</td>
    <tr>
    <td> <a href="2_flat_bottom_magnetic_model/index.html">flat bottom magnetic model </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.209</td>
    <td align="center">6.31</td>
    <td align="center">-3.14</td>
    <td align="center">-8.96</td>
    
    <td align="center">334.372</td>
    <td align="center">12.667</td>
    <td align="center">21.894</td>
    <td align="center">2.334</td>
    
    <td align="center">397.204</td>
    <td align="center">12.667</td>
    <td align="center">21.894</td>
    <td align="center">2.334</td>
    
    <td align="center">403.207</td>
    <td align="center">22.653</td>
    <td align="center">11.637</td>
    <td align="center">3.083</td>
    
    <td align="center">422.616</td>
    <td align="center">12.667</td>
    <td align="center">21.895</td>
    <td align="center">2.334</td>
    
    <td align="center">528.871</td>
    <td align="center">22.653</td>
    <td align="center">11.637</td>
    <td align="center">3.083</td>
    
    <td align="center">510.833</td>
    <td align="center">11.819</td>
    <td align="center">22.257</td>
    <td align="center">2.334</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](bare_machine_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>