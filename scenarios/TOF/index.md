<h1> TOF optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the n_TOF fixed target physics programme. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the TOF cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.139</td>
    <td align="center">6.35</td>
    <td align="center">-5.26</td>
    <td align="center">-7.13</td>
    
    <td align="center">334.372</td>
    <td align="center">12.67</td>
    <td align="center">22.073</td>
    <td align="center">2.738</td>
    
    <td align="center">397.204</td>
    <td align="center">12.498</td>
    <td align="center">22.003</td>
    <td align="center">2.268</td>
    
    <td align="center">403.207</td>
    <td align="center">22.329</td>
    <td align="center">11.699</td>
    <td align="center">3.135</td>
    
    <td align="center">422.616</td>
    <td align="center">13.202</td>
    <td align="center">21.501</td>
    <td align="center">2.649</td>
    
    <td align="center">528.871</td>
    <td align="center">22.684</td>
    <td align="center">11.624</td>
    <td align="center">3.604</td>
    
    <td align="center">510.833</td>
    <td align="center">11.593</td>
    <td align="center">22.346</td>
    <td align="center">2.435</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.138</td>
    <td align="center">6.35</td>
    <td align="center">-5.25</td>
    <td align="center">-7.16</td>
    
    <td align="center">334.372</td>
    <td align="center">12.66</td>
    <td align="center">22.158</td>
    <td align="center">2.625</td>
    
    <td align="center">397.204</td>
    <td align="center">12.489</td>
    <td align="center">21.974</td>
    <td align="center">2.344</td>
    
    <td align="center">403.207</td>
    <td align="center">22.33</td>
    <td align="center">11.658</td>
    <td align="center">3.193</td>
    
    <td align="center">422.616</td>
    <td align="center">13.209</td>
    <td align="center">21.543</td>
    <td align="center">2.569</td>
    
    <td align="center">528.871</td>
    <td align="center">22.689</td>
    <td align="center">11.67</td>
    <td align="center">3.486</td>
    
    <td align="center">510.833</td>
    <td align="center">11.6</td>
    <td align="center">22.282</td>
    <td align="center">2.447</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">19.4</td>
    <td align="center">20.34</td>
    <td align="center">21.68</td>
    <td align="center">1.0</td>
    <td align="center">20.32</td>
    <td align="center">6.131</td>
    <td align="center">6.404</td>
    <td align="center">1.23</td>
    <td align="center">0.87</td>
    
    <td align="center">334.372</td>
    <td align="center">12.826</td>
    <td align="center">21.629</td>
    <td align="center">2.394</td>
    
    <td align="center">397.204</td>
    <td align="center">12.826</td>
    <td align="center">21.629</td>
    <td align="center">2.394</td>
    
    <td align="center">403.207</td>
    <td align="center">22.978</td>
    <td align="center">11.466</td>
    <td align="center">3.162</td>
    
    <td align="center">422.616</td>
    <td align="center">12.826</td>
    <td align="center">21.63</td>
    <td align="center">2.394</td>
    
    <td align="center">528.871</td>
    <td align="center">22.978</td>
    <td align="center">11.466</td>
    <td align="center">3.162</td>
    
    <td align="center">510.833</td>
    <td align="center">11.958</td>
    <td align="center">21.924</td>
    <td align="center">2.394</td>
    <tr>
    <td> <a href="3_extraction/index.html">extraction</a></td>
    <td align="center">19.4</td>
    <td align="center">20.34</td>
    <td align="center">21.68</td>
    <td align="center">1.0</td>
    <td align="center">20.32</td>
    <td align="center">6.11</td>
    <td align="center">6.423</td>
    <td align="center">2.01</td>
    <td align="center">0.94</td>
    
    <td align="center">334.372</td>
    <td align="center">34.021</td>
    <td align="center">18.568</td>
    <td align="center">6.6</td>
    
    <td align="center">397.204</td>
    <td align="center">24.845</td>
    <td align="center">15.173</td>
    <td align="center">-0.327</td>
    
    <td align="center">403.207</td>
    <td align="center">26.132</td>
    <td align="center">13.551</td>
    <td align="center">1.273</td>
    
    <td align="center">422.616</td>
    <td align="center">13.594</td>
    <td align="center">35.882</td>
    <td align="center">5.608</td>
    
    <td align="center">528.871</td>
    <td align="center">36.255</td>
    <td align="center">11.421</td>
    <td align="center">8.207</td>
    
    <td align="center">510.833</td>
    <td align="center">6.57</td>
    <td align="center">41.264</td>
    <td align="center">2.377</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](TOF_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>