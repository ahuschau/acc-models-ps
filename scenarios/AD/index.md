<h1> AD optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the Antiproton Decelerator. Before extraction the PFW are modified to change tunes and chromaticities. The magnetic cycle is displayed in the interactive plot below.  </p>


<object width="500px" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the AD cycle.</p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH54</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV64</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH65</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSH68</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BWSV85</b></th>
    <th id="CELL2" colspan="4" align = center> <b>PR.BGI82</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="0_injection/index.html">injection</a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.128</td>
    <td align="center">6.259</td>
    <td align="center">-5.21</td>
    <td align="center">-7.1</td>
    
    <td align="center">334.372</td>
    <td align="center">12.871</td>
    <td align="center">21.698</td>
    <td align="center">2.677</td>
    
    <td align="center">397.204</td>
    <td align="center">12.228</td>
    <td align="center">22.084</td>
    <td align="center">2.353</td>
    
    <td align="center">403.207</td>
    <td align="center">21.719</td>
    <td align="center">11.804</td>
    <td align="center">3.249</td>
    
    <td align="center">422.616</td>
    <td align="center">14.003</td>
    <td align="center">20.985</td>
    <td align="center">2.715</td>
    
    <td align="center">528.871</td>
    <td align="center">23.302</td>
    <td align="center">11.574</td>
    <td align="center">3.663</td>
    
    <td align="center">510.833</td>
    <td align="center">11.229</td>
    <td align="center">22.639</td>
    <td align="center">2.549</td>
    <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">6.128</td>
    <td align="center">6.259</td>
    <td align="center">-5.2</td>
    <td align="center">-7.12</td>
    
    <td align="center">334.372</td>
    <td align="center">12.854</td>
    <td align="center">21.755</td>
    <td align="center">2.609</td>
    
    <td align="center">397.204</td>
    <td align="center">12.222</td>
    <td align="center">22.029</td>
    <td align="center">2.413</td>
    
    <td align="center">403.207</td>
    <td align="center">21.728</td>
    <td align="center">11.763</td>
    <td align="center">3.308</td>
    
    <td align="center">422.616</td>
    <td align="center">14.006</td>
    <td align="center">21.047</td>
    <td align="center">2.684</td>
    
    <td align="center">528.871</td>
    <td align="center">23.295</td>
    <td align="center">11.617</td>
    <td align="center">3.61</td>
    
    <td align="center">510.833</td>
    <td align="center">11.241</td>
    <td align="center">22.59</td>
    <td align="center">2.572</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">25.48</td>
    <td align="center">26.42</td>
    <td align="center">28.15</td>
    <td align="center">1.0</td>
    <td align="center">26.4</td>
    <td align="center">6.263</td>
    <td align="center">6.242</td>
    <td align="center">0.71</td>
    <td align="center">1.1</td>
    
    <td align="center">334.372</td>
    <td align="center">12.561</td>
    <td align="center">22.091</td>
    <td align="center">2.295</td>
    
    <td align="center">397.204</td>
    <td align="center">12.561</td>
    <td align="center">22.091</td>
    <td align="center">2.295</td>
    
    <td align="center">403.207</td>
    <td align="center">22.435</td>
    <td align="center">11.764</td>
    <td align="center">3.03</td>
    
    <td align="center">422.616</td>
    <td align="center">12.56</td>
    <td align="center">22.092</td>
    <td align="center">2.295</td>
    
    <td align="center">528.871</td>
    <td align="center">22.435</td>
    <td align="center">11.764</td>
    <td align="center">3.03</td>
    
    <td align="center">510.833</td>
    <td align="center">11.725</td>
    <td align="center">22.507</td>
    <td align="center">2.295</td>
    <tr>
    <td> <a href="3_before_extraction/index.html">before extraction </a></td>
    <td align="center">25.48</td>
    <td align="center">26.42</td>
    <td align="center">28.15</td>
    <td align="center">1.0</td>
    <td align="center">26.4</td>
    <td align="center">6.157</td>
    <td align="center">6.245</td>
    <td align="center">-1.03</td>
    <td align="center">0.39</td>
    
    <td align="center">334.372</td>
    <td align="center">12.802</td>
    <td align="center">22.024</td>
    <td align="center">2.375</td>
    
    <td align="center">397.204</td>
    <td align="center">12.802</td>
    <td align="center">22.024</td>
    <td align="center">2.375</td>
    
    <td align="center">403.207</td>
    <td align="center">22.784</td>
    <td align="center">11.784</td>
    <td align="center">3.129</td>
    
    <td align="center">422.616</td>
    <td align="center">12.802</td>
    <td align="center">22.025</td>
    <td align="center">2.375</td>
    
    <td align="center">528.871</td>
    <td align="center">22.784</td>
    <td align="center">11.784</td>
    <td align="center">3.129</td>
    
    <td align="center">510.833</td>
    <td align="center">11.944</td>
    <td align="center">22.429</td>
    <td align="center">2.375</td>
    <tr>
    <td> <a href="4_extraction/index.html">extraction</a></td>
    <td align="center">25.48</td>
    <td align="center">26.42</td>
    <td align="center">28.15</td>
    <td align="center">1.0</td>
    <td align="center">26.4</td>
    <td align="center">6.124</td>
    <td align="center">6.252</td>
    <td align="center">2.6</td>
    <td align="center">0.01</td>
    
    <td align="center">334.372</td>
    <td align="center">35.196</td>
    <td align="center">17.849</td>
    <td align="center">6.308</td>
    
    <td align="center">397.204</td>
    <td align="center">24.414</td>
    <td align="center">16.966</td>
    <td align="center">0.074</td>
    
    <td align="center">403.207</td>
    <td align="center">24.613</td>
    <td align="center">12.255</td>
    <td align="center">1.788</td>
    
    <td align="center">422.616</td>
    <td align="center">14.905</td>
    <td align="center">30.041</td>
    <td align="center">5.647</td>
    
    <td align="center">528.871</td>
    <td align="center">40.081</td>
    <td align="center">13.314</td>
    <td align="center">8.152</td>
    
    <td align="center">510.833</td>
    <td align="center">5.721</td>
    <td align="center">30.97</td>
    <td align="center">2.799</td>
    </tr>
 
</table>

The basis for the beam-based models are non-linear chromaticity measurements. A summary of the measurements and the analysis for this scenario is available [here](AD_chromaticity_measurement.md){target=_blank}.

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>