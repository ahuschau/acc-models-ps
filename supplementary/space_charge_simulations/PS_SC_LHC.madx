/**********************************************************************************
*
* MAD-X input script for space charge simulations using the LHC proton flat bottom 
* optics (with low-chromaticity). A large number of elements are removed from the 
* sequence to decrease tracking time.
*
* 22/08/2019 - Alexander Huschauer
************************************************************************************/
 
/******************************************************************
 * Energy and particle type definition
 ******************************************************************/

BEAM, PARTICLE=PROTON, PC = 2.14;
BRHO      := BEAM->PC * 3.3356;

/******************************************************************
 * Call lattice files
 ******************************************************************/

call, file="../../PS_MU.seq";
call, file="../../PS_SS.seq";
call, file="../../PS.str";
call, file="../../scenarios/LHC_PROTON/1_flat_bottom/PS_FB_LHC.str";

/******************************************************************
 * SEQEDIT to remove unwanted elements
 ******************************************************************/
 
use, sequence = PS;
twiss, file = 'initial_lattice.tfs';

seqedit,sequence = PS;
        flatten;
endedit;

seqedit,sequence = PS;
        call, file = 'remove_elements.seq';
        remove, element=SELECTED;
endedit;

/******************************************************************
 * Cycle the sequence to a different initial location if desired
 ******************************************************************/

/*
START_LATTICE: MARKER;

seqedit,sequence = PS;
        flatten;
        REPLACE, ELEMENT=PR.BWSH65, BY=START_LATTICE;
        cycle , start = START_LATTICE;
endedit;
*/

/******************************************************************
 * Create flat file
 ******************************************************************/

use, sequence = PS;
twiss, file = 'simplified_lattice.tfs';

ptc_create_universe;
ptc_create_layout,time=true, model=2, exact=true, method=6, nst=5;
ptc_script, file="./print_flat_file.ptc"; 
ptc_end;
